package highscore;

import java.util.*;
import java.io.Serializable;

class Score implements Comparable<Score>{
    private int score;
    private String name;

    public int getScore() {
        return score;
    }

    public String getName() {
        return name;
    }

    public Score(String name, int score) {
        this.score = score;
        this.name = name;
    }

    public int compareTo(Score score1) {
        return score1.getScore()-this.getScore();
    }
}
