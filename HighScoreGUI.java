package highscore;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import java.awt.FlowLayout;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.DefaultTableCellRenderer;

public class HighScoreGUI extends JFrame {

    DefaultTableModel model;
    JTable table;
    String col[] = {"Name", "Score"};
    static HighScoreManager hsm = new HighScoreManager();

    public HighScoreGUI(){

        hsm.addScore("Bob", 25000);
        hsm.addScore("Gen", 12398);
        hsm.addScore("Matt", 71628);
        hsm.addScore("Matt", 71628);
        hsm.addScore("Matt", 71628);
        hsm.addScore("Matt", 71628);
        hsm.addScore("Matt", 71628);
        hsm.addScore("Matt", 71628);
        hsm.addScore("Matt", 71628);
        hsm.addScore("Matt", 71628);

        model = new DefaultTableModel(col,hsm.size()+10);
        table=new JTable(model){

            @Override
            public boolean isCellEditable(int arg0, int arg1) {
                return false;
            }

        };

        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment( JLabel.CENTER );
        table.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
        table.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );


        for(int i = 0; i < hsm.size(); i++){
            table.setValueAt(hsm.getScores().get(i).getName(),i,0);
            table.setValueAt(hsm.getScores().get(i).getScore(),i,1);
        }

        JScrollPane pane = new JScrollPane(table);

        add(pane);
        setTitle("Highscores!!");
        setSize(300,400);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable(){
            @Override
            public void run(){
                HighScoreGUI hsg = new HighScoreGUI();
                hsg.setVisible(true);
            }
        });
    }
}