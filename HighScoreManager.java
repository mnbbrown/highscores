package highscore;

import java.util.*;
import java.io.Serializable;

//TODO: Clean Array to ensure it is only 10 in length;
class HighScoreManager{

    private List<Score> scores;

    public HighScoreManager(){
        scores = new ArrayList<Score>();
    }

    public List<Score> getScores(){
        sort();
        return scores;
    }

    public int size(){
        return this.scores.size();
    }

    public String String(){
        String buffer = "";
        int size = this.scores.size();
        if (size > 10) {
            size = 10;
        }
        for(int i=0; i < size; i++){
            buffer += scores.get(i).getName() + " " + scores.get(i).getScore() + "\n";
        }
        return buffer;
    }

    private void sort(){
        Collections.sort(this.scores);
    }

    public void addScore(String nm, int score){
        this.scores.add(new Score(nm, score));
        this.sort();
        if (this.scores.size() > 10) {
            this.scores = this.scores.subList(0,10);
        }
    }

    public static void main(String[] args) {
        HighScoreManager hsm = new HighScoreManager();
        hsm.addScore("Bob", 25000);
        hsm.addScore("Gen", 12398);
        hsm.addScore("Matt", 71628);
        hsm.addScore("Matt", 71628);
        hsm.addScore("Matt", 71628);
        hsm.addScore("Matt", 71628);
        hsm.addScore("Matt", 71628);
        hsm.addScore("Matt", 71628);
        hsm.addScore("Matt", 71628);
        hsm.addScore("Matt", 71628);
        hsm.addScore("Fred", 12879);
        hsm.addScore("Fred", 12879);
        hsm.addScore("Fred", 12879);
        hsm.addScore("Fred", 12879);
        hsm.addScore("Fred", 12879);
        hsm.addScore("Fred", 12879);
        hsm.addScore("Fred", 12879);
        System.out.println(hsm.String());
    }

}